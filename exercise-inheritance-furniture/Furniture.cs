﻿
using System;
using System.Collections.Generic;
using System.Text;
namespace exercise_inheritance_furniture
{
   public class Furniture
    {
        //Add below mentioned Properties 
        //Height(int),Width(int),Color(string),Price(double),Qty(int)
        public int Height;
        public int Width;
        public string Color;
        public double Price;
        public int Qty;

        public virtual void Accept()
        {

            Console.WriteLine("---- Please enter the Height------");
            Height = int.Parse(Console.ReadLine());
            Console.WriteLine("---- Please enter the Width------");
            Width = int.Parse(Console.ReadLine());
            Console.WriteLine("---- Please enter the Color------");
            Color = Console.ReadLine();
            Console.WriteLine("---- Please enter the price------");
            Price = double.Parse(Console.ReadLine());
            Console.WriteLine("---- Please enter the Quantity------");
            Qty = int.Parse(Console.ReadLine());



        }
        public virtual void Display()
        {
            //code to display height width ,price,qty and color
            Console.WriteLine($"The Height of the Furniture is       : {Height}");
            Console.WriteLine($"The Width of the Furniture is        : {Width}");
            Console.WriteLine($"The Color of the Furniture is        : {Color}");
            Console.WriteLine($"The Price of the Furniture is        : {Price}");
            Console.WriteLine($"The Quantity of the Furniture is     : {Qty}");
        } 
    }
}






























/**
* @author Anilkumar.S
*
* @date - 8/24/2020 2:24:45 PM 
*/
