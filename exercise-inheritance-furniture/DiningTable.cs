﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace exercise_inheritance_furniture
{
    //child of furniture
   public class DiningTable: Furniture
    {
        //Add  NoOfLegs  property
        //overrride the Accept method to read NoOfLegs property also
        //
        //override the Display method to include the display of NoOfLegs property also
        public int NoOfLegs;

        public override void Accept()
        {
            base.Accept();
            Console.WriteLine("---- Please Enter the No of Legs Required---------");
            NoOfLegs = int.Parse(Console.ReadLine());
        }

        public override void Display()
        {
            base.Display();
            Console.WriteLine($"The total Legs of the Furniture is   : {NoOfLegs}");
        }
    }
}


















































/**
* @author Anilkumar.S
*
* @date - 8/24/2020 2:27:28 PM 
*/
