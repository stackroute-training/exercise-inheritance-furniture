﻿using System;
using System.Dynamic;

namespace exercise_inheritance_furniture
{
   public class Program
    {
        static void Main(string[] args)
        {
            //create an array  of objects in furniture type
            //create program class object
            // call AddToStock(<furniture array>);
            //call ShowStockDetails(<furniture array>);
            // call TotalStockValue(<furniture array>) and display output 
            Furniture[] f = new Furniture[5];
            Program program = new Program();
            program.AddToStock(f);
            program.ShowStockDetails(f);
            Console.WriteLine(program.TotalStockValue(f));
        }
       public  int AddToStock(Furniture []items) {

            //accept the choice from user for type of furniture(BookShelf  or DiningTable)
            // and according to the choice create necessary sub class object and store data
            //continue the process up to the size of array
            // return <size of array>;
            Console.WriteLine("----- Choose How Many want to add to cart. The maximum No of Products can be added to Stock are 5 ----");
            int val1 = int.Parse(Console.ReadLine());
            if(val1 <= 5 && val1 > 0)
            {
                for (int i = 0; i < val1; i++)
                {
                    Console.WriteLine("--- please choose the following ---");
                    Console.WriteLine("Press 1 for Bookshelf");
                    Console.WriteLine("Press 2 for DiningTable");
                    int val = int.Parse(Console.ReadLine());
                    switch (val)
                    {
                        case 1:
                            BookShelf book = new BookShelf();
                            book.Accept();
                            items[i] = new BookShelf() { Height = book.Height, Width = book.Width, Color = book.Color, NoOfShelves = book.NoOfShelves, Price = book.Price, Qty = book.Qty };

                            break;
                        case 2:
                            DiningTable table = new DiningTable();
                            table.Accept();
                            items[i] = new DiningTable() { Height = table.Height, Width = table.Width, Color = table.Color, NoOfLegs = table.NoOfLegs, Price = table.Price, Qty = table.Qty };
                            break;
                        default:
                            Console.WriteLine("Please choose correct option");
                            break;
                    }
                }
                return val1;
            }
            else
            {
                Console.WriteLine("--- Please Try Again -----");
                return 0;
            }
            
            
        }
     public   double TotalStockValue(Furniture []items)
        {
            double stockValue=default;
            //calculate total stock value i.e qty * price of each item
            
            foreach(Furniture item in items)
            {
                //if(item != null)
                //{
                    stockValue += (item.Price * item.Qty);
               // }
            }
            return stockValue;
        }
    public    int ShowStockDetails(Furniture[] items)
        {
            //call display method from all object to display the stock 
            //return <size of array>;
            foreach (Furniture item in items)
            {
                item.Display();
            }
            
            return items.Length;
        }
    }
}
